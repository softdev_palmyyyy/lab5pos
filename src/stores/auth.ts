import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currnetUser = ref<User>({
      id: 1,
      email: 'mana123@gmail.com',
      password: 'Pass@1234',
      fullName: 'มานะ งานดี',
      gender: 'male',
      roles: ['user']
  })

  return { currnetUser }
})
